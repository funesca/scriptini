#! /bin/bash

# simple script to get annoying notifications
# to remind myself to charge my laptop

while :
do
	perc=$(acpi -b | cut -d " " -f 4 | tr -d "%" | tr -d ",")
	time=$(acpi -b | cut -d " " -f 5)
	status=$(cat /sys/class/power_supply/BAT0/status)

	if [[ $status == "Discharging" && $perc -le 30 ]]; then
		notify-send -u critical -w -a "low battery level"\
			"battery at $perc%, connect charger" "remaining time $time" &
	fi

	sleep 5m
done

