#!/bin/bash

set -e

case "$1" in
  "list")
    data=$(sed '0,/^__DATA__$/d' "$0")
    echo "$data"
    ;;
  "copy")
    input=$(tee)
    if [ ! -z "$input" ]; then
        emoji=${input: -1}
        echo -n "$emoji" | xclip -selection c
        command -v notify-send > /dev/null && notify-send -t 200 "$emoji copied!"
    fi
    ;;
  "")
    #bash $0 list | dmenu -l 6 | bash $0 copy
    #bash $0 list | rofi -dmenu -theme ~/.config/rofi/configvim.rasi | bash $0 copy
    bash $0 list | rofi -dmenu -theme ~/.config/rofi/configvim.rasi
    ;;
esac

exit

__DATA__
:           console command
o,t,w       open a page in current tab, new tab or new window
O,T,W       similar to o,t,w but that contains current URL
b           select open tabs by URL or title
a           add bookmark
h,j,k,l     scroll left, down, up, right
0,$         scroll a page to the leftmost/rightmost
gg,G        scroll to top, bottom
d           close current tab
x$          close all tabs to the right
u           reopen closed tab
K,J         go to previous, next tab
ctrl+6      go to previously selected tab
r,R         reload, force reload
zp          toggle pin
zi,zo,zz    zoom in, zoom out, reset zooom
f,F         start following links, opens in new tab
H,L         go back/forward in the tab history
m,'         mark tab, jump to mark
gi          focus input
gh,gH       go to home, in a new tab
gf          page source
y,p,P       yank URL, paste in current/new tab
/           find in page
n,N         find next, previous
shift+esc   toggle addon
