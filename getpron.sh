#! /bin/bash

# stupid script to get a word (UK) pronunciation
# and definition from the cambridge dictionary online

url="https://dictionary.cambridge.org/dictionary/english/"
word=${@: -1}

function usage {
	echo -e "Usage: getpron.sh [OPTIONS] WORD"
	echo -e "-a\t\tprint all"
	echo -e "-d\t\tprint only definition"
	echo -e "-l\t\tprint definition with legend"
	echo -e "-p\t\tprint only pronunciation"
	echo -e "-u\t\tprint only ugly pronunciation"
}

if [[ ${#} -eq 0  ]]; then
    usage
	exit -1
fi

if [[ $word == -* ]]; then
	usage
	exit -1
fi

d_flag=''
l_flag=''
p_flag=''
u_flag=''

while getopts 'adlpuh' flag; do
	case "${flag}" in
		a) d_flag='true'
			l_flag='true'
			p_flag='true';;
		d) d_flag='true';;
		l) l_flag='true';;
		p) p_flag='true';;
		u) u_flag='true';;
		h) usage
			exit 0;;
		?) exit -1;;
	esac
done

if [[ $d_flag = '' && $p_flag == '' && $l_flag == '' && $u_flag == '' ]]; then
	d_flag='true'
	p_flag='true'
	l_flag='true'
fi

if [[ $p_flag  =~ "true" ]]; then
	ukpron=$(curl -s $url$word | grep -m 1 "pron dpron" | sed 's/<[^<>]*>//g' | sed 's/ us//g')
	echo $ukpron
elif [[ $u_flag =~ "true" ]]; then
	ukpron=$(curl -s $url$word | grep -m 1 "pron dpron" | sed 's/<span class="sp dsp">\(.\)<\/span>/<sup>\1<sup\/>/g' | sed 's/<[^<>u]*>//g' | sed 's/<span class="us dpron-i ">us<span class="daud">//g')
	echo $ukpron
fi

if [[ $d_flag =~ "true" ]]; then
	def=$(curl -s $url$word | grep "def ddef_d db" | sed 's/<[^<>]*>//g')
	echo $def | sed 's/[ABC][0-9]/\n&/g' | sed 's/: /\n&/g' | sed '/^:/d' | sed 's/ Note//g'

	if [[ $l_flag =~ "true" ]]; then

		def=$(echo $def | sed 's/[ABC][0-9]/\n&/g' | sed 's/: /\n&/g' | sed '/^:/d' | sed 's/ Note//g')

		echo

		if [[ $def =~ "[ C" || $def =~ "C ]" ]]; then
			echo -e [ C ]'\t'countable noun
		fi
		if [[ $def =~ "[ U" || $def =~ "U ]" ]]; then
			echo -e [ U ]'\t'uncountable or singular noun
		fi
		if [[ $def =~ "[ S" || $def =~ "S ]" ]]; then
			echo -e [ S ]'\t'singular noun
		fi
		if [[ $def =~ "[ T" || $def =~ "T ]" ]]; then
			echo -e [ T ]'\t'transitive verb
		fi
		if [[ $def =~ "[ I" || $def =~ "I ]" ]]; then
			echo -e [ I ]'\t'intransitive verb
		fi
		if [[ $def =~ "[ L" || $def =~ "L ]" ]]; then
			echo -e [ L ]'\t'linking verb
		fi
	fi
fi
